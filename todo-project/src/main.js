import { createApp } from 'vue'
import App from './App.vue'

//Styling CSS
import "bootstrap/dist/css/bootstrap.min.css";
import "bootstrap-icons/font/bootstrap-icons.css";

createApp(App).mount('#app')
